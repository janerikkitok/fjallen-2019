# Fjällen 2019
## Vandring
Vandring kungsleden från Saltoluokta till Kvikkjokk (eller Kvikkjokk - Saltoluokta), ca 72 km.

### Resa

Burre och PG möter upp med Janne i Gällivare på morgonen 1 Juli. Tobias kommer antingen med från Luleå eller möter upp i Gällivare. Barre ska på kung fu läger i Tyskland 6e Juli.

Buss går från Gällivare kl 13:45, framme vid båten 15:30. Båten avgår 15:35 enligt tidtabell men väntar in bussen.

I Kvikkjokk avgår bussen 15:15 på vardagar. Byte i Jokkmokk, avgång från Jokkmokk 20:10.

### Etapper

#### Dag 1 (Efter båten): Saltoluokta - Avtsusjvágge (8 km)
Cirka 8 km enligt mätverktyget i lantmäteriets tjänst. Kanske kan en tidigare lägerplats tas, ser ut att finnas några bäckar tidigare.

#### Dag 2: Avtsusjvágge - Aktse (21 km)
Denna etappen innehåller rodden, potential att vara dryg som fan. Kanske att en tidigare lägerplats är lämplig.
Schema för båten i Aktse:

*21/6-23/9:*

Aktse – Laitaure 09.00 och 17.00

Laitaure – Aktse 09.15 och 17.15

Pris: 200 kr
#### Dag 3 Aktse - Pårtsestugan (17 km)
Eventuellt lite brant i början av etappen, i övrigt ganska chill.

#### Dag 4 Pårtestugan - Kvikkjokk (14 km)
Ser inte ut att vara några nämnvärda höjdmeter på kartan, mycket skog. Av erfarenhet vet vi att skogarna kring Kvikkjokk är jävla dryga :D.

## Fiske
Flugfiske i Kaitumområdet. Även annat fiske kan vara aktuellt.

PG behöver vara i Luleå 10e Juli på eftermiddagen. Om vi utgår från att vi är klara med vandringen fredagen den 5e så har vi 4 fiskedagar (lör, sön, mån, tis).

### Kaitumälven nedströms Killinge

Förslaget är att vi fiskar i Kaitumälven nedströms Killingefallet. Det går att köra med bil hela vägen till Killingefallet. Från Killingefallet till Gruvselet är det ca 5 km att gå. Vid Gruvselet finns en "rastplats" samt en torrtoa. Sträckan mellan Killingefallet ser ut att ha mycket forsar.

Det går även att ta sig till södra älvstranden från vägen (ca 3 km att gå). Detta är enligt [hemsidan](http://www.kaitumalven.nu/fiskeplatser.html) en populär fiskeplats.
Särskilt fiskekort behövs, 150 kr / dag eller 200 kr / 3 dagar, se länk.

Ungefär halvvägs till Gruvselet ligger "Svartselet". [Sträckan mellan Svartselet och  Gruvselet är enbart för flugfiske, här är det även förbjudet att ta upp fisk.](http://www.kaitumalven.nu/fiskebestam.html)

En put and take sjö som heter Ahvenjärvi ingår även i fiskekortet.
### Andra förslag

Om vi blir less på att fiska bara i Kaitumälven så hade något alternativ (någon sjö kanske?) varit intressant.


### Kartor

#### Karta Kaitumälven nedströms Killinge
Blått är parkeringsplatsen, röd ruta är flugfiskeområdet.
![Karta](bilder/kaitum_fiske.png)


### Resa

Med Burres bil från Gällivare (finns det hjärterum finns det stjärterum!).
Se fiskekortsbilaga i länkar.

## Länkar

[Lantmäteriet karta](https://kso.etjanster.lantmateriet.se/#)

[Länstrafiken norrbotten](https://ltnbd.se/)

[Fiskekort Gällivare](https://www.lansstyrelsen.se/download/18.299c1cd2167c07c64754bb/1545136683638/Ga%CC%88llivare_2019_oversikt_WEBB.pdf)

[Fiskekort Kaitumälven](https://www.fiskekort.se/kaitumalven/)

[Fjällkartor (finns inte i lantmäteriets tjänst längre)](http://www.bengt.nolang.se/kartor/Fjallkartan/Kartblad.aspx)
